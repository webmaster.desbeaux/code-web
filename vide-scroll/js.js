//https://j11y.io/javascript/regex-selector-for-jquery/
//James Padolsey
$.expr[':'].regex = function(elem, index, match) {
    var matchParams = match[3].split(','),
        validLabels = /^(data|css):/,
        attr = {
            method: matchParams[0].match(validLabels) ? 
                        matchParams[0].split(':')[0] : 'attr',
            property: matchParams.shift().replace(validLabels,'')
        },
        regexFlags = 'ig',
        regex = new RegExp(matchParams.join('').replace(/^\s+|\s+$/g,''), regexFlags);
    return regex.test(jQuery(elem)[attr.method](attr.property));
}

var $window = $(window);
//var $elementsAllScroll = $(".vide-scroll")

//Recherche si l'élément est dans la fenetre
function isScrolledIntoView($elem, $window) {
    var docViewTop = $window.scrollTop();
    var docViewBottom = docViewTop + $window.height();

    var elemTop = $elem.offset().top;
    var elemBottom = elemTop + $elem.height();

    return elemTop<=docViewBottom;

    // if ($elem.height() >= ($window.height())){
    //     // console.log("élément trop grand" + $elem.html())
    //     diff = $elem.height()-$window.height()
    //     return (docViewTop+($elem.height()/2))>=elemTop;
    // }
    // return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
}

$(document).on("scroll", function () {
    //Récupération de tous les éléments scrollable
    elements = $(".vide-scroll:regex(class, vide_)")
    //Parcours des éléments pour savoir si il faut appliquer un effet
        //console.log(elements)
    elements.each(function(index){
        element = elements.eq(index)
        //console.log(element)
        //Si il est dans la vue
        if (isScrolledIntoView(element, $window)) {
            //récupération de la class pour ajouter _effect à la fin afin de faire l'effet voulu
            elementClassList = $(element)[0].classList
            //console.log("---------------")
            //console.log(elementClassList)
            var result = ""
            var noEffect = true

            for (var i = 0; i < elementClassList.length; i++) {
                //console.log(elementClassList[i])
                if (elementClassList[i].startsWith("vide_")){
                    result += elementClassList[i] + "_effect "
                    //console.log("result"+result)
                }
                if (elementClassList[i].endsWith("_effect")){
                    noEffect = false
                    //console.log("noEffect"+noEffect)
                }
            }
            if (result != "" && noEffect){
                element.delay(500).queue(function(){
                    $(this).addClass(result)
                })
            }
        }
    })
});

$(document).ready(function(){
    elements = $(".vide-scroll:regex(class, vide_)")
    //Parcours des éléments pour savoir si il faut appliquer un effet
        //console.log(elements)
    elements.each(function(index){
        element = elements.eq(index)
        elementParent = element.parent()
        if (! elementParent.hasClass('vide-scroll'))
            elementParent.addClass("vide-scroll-parent")
    })
})