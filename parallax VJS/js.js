
// If you have some tips or improvement, please let me know.
let parallaxDiv = document.querySelectorAll(".img-parallax");

// console.log(parallaxDiv)
for (let i = 0; i < parallaxDiv.length; i++){
    let img = parallaxDiv.item(i);
    let imgParent = img.parentNode;
    function parallaxImg () {
        let speed = img.dataset.speed;
        let imgY = imgParent.offsetTop;
        let winY = window.scrollY;
        let winH = img.height;
        let parentH = imgParent.scrollHeight;

        // The next pixel to show on screen
        let winBottom = winY + winH;

        // If block is shown on screen
        if (winBottom > imgY && winY < imgY + parentH) {
            // Number of pixels shown after block appear
            let imgBottom = ((winBottom - imgY) * speed);
            // Max number of pixels until block disappear
            let imgTop = winH + parentH;
            // Porcentage between start showing until disappearing
            var imgPercent = ((imgBottom / imgTop) * 100) + (50 - (speed * 50));
        }
        // console.log(imgPercent)
        img.style.top =  imgPercent + '%'
        img.style.transform =  'translate(-50%, -' + imgPercent + '%)'
    }

    document.addEventListener("DOMContentLoaded", function() {
        parallaxImg();
    });
    document.addEventListener("scroll", function() {
        parallaxImg();
    });
}